from sqlalchemy import (
    create_engine,
)

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

engine = create_engine("sqlite:///blog.db", echo=True)
Base = declarative_base(bind=engine)

Session = scoped_session(sessionmaker(bind=engine))


def init_db():
    import app.models
    Base.metadata.create_all()


