from datetime import datetime

from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Text
from sqlalchemy.orm import relationship, backref

from app.db import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String(64), unique=True, nullable=False)
    email = Column(String(120), unique=True, nullable=False)
    first_name = Column(String(120), nullable=False, default="")
    last_name = Column(String(120), nullable=False, default="")

    def __str__(self):
        return f"<User ({self.id=}, {self.username=})>"

    def __repr__(self):
        return str(self)


class Tag(Base):
    __tablename__ = "tag"

    id = Column(Integer, primary_key=True)
    name = Column(String(64), nullable=False)
    color = Column(String(6), nullable=False, default='a6a6a6', comment='Hex color without #')

    def __str__(self):
        return f"<Tag ({self.id=}, {self.name=})>"

    def __repr__(self):
        return str(self)


class Tags(Base):
    __tablename__ = "tags"

    tag_id = Column(Integer, ForeignKey('tag.id'), primary_key=True)
    post_id = Column(Integer, ForeignKey('posts.id'), primary_key=True)


class Category(Base):
    __tablename__ = "categories"

    id = Column(Integer, primary_key=True)
    name = Column(String(64), nullable=False)

    def __str__(self):
        return f"<Category ({self.id=}, {self.name=})>"

    def __repr__(self):
        return str(self)


class Post(Base):
    __tablename__ = "posts"

    id = Column(Integer, primary_key=True)
    title = Column(String(90), nullable=False)

    date_posted = Column(DateTime, nullable=False, default=datetime.utcnow)
    content = Column(Text, nullable=False)

    anilist_id = Column(Integer, index=True, nullable=True)
    mal_id = Column(Integer, index=True, nullable=True)

    tags = relationship(Tag, secondary='tags', backref=backref('posts', lazy=True))

    category_id = Column(Integer, ForeignKey('categories.id'), nullable=False)
    category = relationship(Category, backref=backref('posts', lazy=True))

    author_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    author = relationship(User, backref=backref('posts', lazy=True))

    def __str__(self):
        return f"<Post ({self.id=}, {self.user_id=}, {self.title=})>"

    def __repr__(self):
        return str(self)
