from app.db import init_db, Session

# Press the green button in the gutter to run the script.
from app.models import User

if __name__ == '__main__':
    init_db()
    session = Session()

    user = User(username="admin", email="admin@admin.com")

    session.add(user)

    session.commit()

    session.close()

